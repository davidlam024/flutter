import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../data/Ressource.dart';

class RessourceDetailPage extends StatefulWidget {
  final String resourceId;

  // Constructeur
  RessourceDetailPage({Key? key, required this.resourceId}) : super(key: key);

  @override
  _RessourceDetailPageState createState() => _RessourceDetailPageState();
}

class _RessourceDetailPageState extends State<RessourceDetailPage> {
  @override
  void initState() {
    super.initState();
    _fetchRessources();
  }

  Future<void> _fetchRessources() async {
    // Implémentez la logique pour récupérer les détails de la ressource avec l'ID widget.resourceId
    // Vous pouvez utiliser widget.resourceId pour accéder à l'ID passé comme argument
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Détails de la ressource'),
      ),
      body: Center(
        child: Text('Ressource ID: ${widget.resourceId}'), // Affichez l'ID de la ressource
      ),
    );
  }
}
