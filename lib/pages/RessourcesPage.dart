import 'package:flutter/material.dart';
import '../data/Ressource.dart';

import 'dart:convert';
import 'package:http/http.dart' as http;

    class RessourcesPage extends StatefulWidget {
      @override
      _RessourcesPageState createState() => _RessourcesPageState();
    }
    class _RessourcesPageState extends State<RessourcesPage> {
      List<Ressource> _ressources = [];

      @override
      void initState() {
        super.initState();
        _fetchRessources();
      }

      Future<void> _fetchRessources() async {
        final apiUrl = 'http://localhost:8080/ressources';
        try {
          final response = await http.get(Uri.parse(apiUrl));

          if (response.statusCode == 200) {
            final String processedJson = replaceNullValues(response.body);
            final List<dynamic> jsonData = json.decode(processedJson);

            List<Ressource> ressources =
            jsonData.map((data) => Ressource.fromJson(data)).toList();

            setState(() {
              _ressources = ressources;
            });
          } else {
            print('Failed to load ressources: ${response.statusCode}');
          }
        } catch (e) {
          print('Error while fetching ressources: $e');

        }
      }

      String replaceNullValues(String inputJson) {
        return inputJson.replaceAllMapped(
          RegExp(r'"\w+":\s*null'), // Recherche de toutes les occurrences de clés suivies de "null"
              (match) => match.group(0)!.replaceFirst("null", "\"\""), // Remplace "null" par une chaîne vide
        );
      }

      @override
      Widget build(BuildContext context) {
        return Scaffold(
          appBar: AppBar(
            title: Text('Liste des Ressources'),
          ),
          body: _ressources.isEmpty
              ? Center(
            child: CircularProgressIndicator(),
          )
              : ListView.builder(
            itemCount: _ressources.length,
            itemBuilder: (context, index) {
              final ressource = _ressources[index];
              return ListTile(
                title: Text(ressource.title),
                subtitle: Text(ressource.description),
                // title: Text("ressource.title"),
                // subtitle: Text("ressource.description"),
              );
            },
          ),
        );
      }
    }


