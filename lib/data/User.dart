// import 'Role.dart';
// import 'Ressource.dart';
// import 'dart:convert';
//
// class User {
//   final int id;
//   final String firstName;
//   final String lastName;
//   final String password;
//   final String email;
//   final Role role;
//   final bool actif;
//   final DateTime birthDate;
//   final List<Ressource> ressources;
//   final String token;
//
//   User({
//     required this.id,
//     required this.firstName,
//     required this.lastName,
//     required this.password,
//     required this.email,
//     required this.role,
//     required this.actif,
//     required this.birthDate,
//     required this.ressources,
//     required this.token,
//   });
//
//   factory User.fromJson(Map<String, dynamic> json) {
//     List<Ressource> ressourcesList = [];
//     if (json['ressources'] != null) {
//       ressourcesList = (json['ressources'] as List<dynamic>)
//           .map((ressourceJson) => Ressource.fromJson(ressourceJson))
//           .toList();
//     }
//     return User(
//         id: json['id'] as int,
//         firstName: json['firstName'] != null ? json['firstName'] as String : "",
//         lastName: json['lastName'] != null ? json['lastName'] as String : "",
//         password: json['password'] != null ? json['password'] as String : "",
//         email: json['email'] != null ? json['email'] as String : "",
//         role: RoleExtension.fromString(json['role'] as String),
//         actif: json['actif'] as bool,
//         birthDate: DateTime.parse(json['birthDate'] as String),
//         ressources: ressourcesList,
//         token: json['token'] != null ? json['token'] as String : ""
//     );
//   }
//
//
// }


class User {
  final int id;
  final String firstName;
  final String lastName;
  final String password;
  final String email;
  final String role;
  final bool actif;
  final DateTime birthDate;

  User({
    required this.id,
    required this.firstName,
    required this.lastName,
    required this.password,
    required this.email,
    required this.role,
    required this.actif,
    required this.birthDate,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'],
      firstName: json['firstName'],
      lastName: json['lastName'],
      password: json['password'],
      email: json['email'],
      role: json['role'],
      actif: json['actif'],
      birthDate: DateTime.parse(json['birthDate']),
    );
  }
}