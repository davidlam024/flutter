enum StatusPublication {
  WAIT,
  REJECTED,
  ACCEPTED
}

extension StatusPublicationExtension on StatusPublication {
  String get value {
    switch (this) {
      case StatusPublication.WAIT:
        return 'WAIT';
      case StatusPublication.REJECTED:
        return 'REJECTED';
      case StatusPublication.ACCEPTED:
        return 'ACCEPTED';
      default:
        throw Exception('Invalid status publication');
    }
  }

  static StatusPublication fromString(String value) {
    switch (value) {
      case 'WAIT':
        return StatusPublication.WAIT;
      case 'REJECTED':
        return StatusPublication.REJECTED;
      case 'ACCEPTED':
        return StatusPublication.ACCEPTED;
      default:
        throw Exception('Invalid status publication string');
    }
  }

  static StatusPublication fromJson(dynamic json) {
    if (json is String) {
      return fromString(json);
    }
    throw Exception('Invalid status publication JSON');
  }
}