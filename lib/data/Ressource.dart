import 'Category.dart';
// import 'StatusPublication.dart';
import 'User.dart';
//
// class Ressource {
//   final int id;
//   final String title;
//   final String description;
//   final DateTime creationDate;
//   final DateTime eventStart;
//   final DateTime eventEnd;
//   final String file;
//   final User utilisateur;
//   final String etat;
//   final Category category; // Ajout de l'attribut de catégorie
//
//   Ressource({
//     required this.id,
//     required this.title,
//     required this.description,
//     required this.creationDate,
//     required this.eventStart,
//     required this.eventEnd,
//     required this.file,
//     required this.utilisateur,
//     required this.etat,
//     required this.category, // Initialisation de l'attribut de catégorie
//   });
//
//   factory Ressource.fromJson(Map<String, dynamic> json) {
//     return Ressource(
//         id: json['id'] as int,
//         title: json['title'] != null ? json['title'] as String : "",
//         description: json['description'] != null ? json['description'] as String : "",
//         creationDate: json['creationDate'] != null ? DateTime.parse(json['creationDate'] as String) : DateTime.now(),
//         eventStart: DateTime.parse(json['eventStart'] as String),
//         eventEnd: DateTime.parse(json['eventEnd'] as String) ,
//         file: json['file'] != null ? json['file'] as String : "",
//         utilisateur: User.fromJson(json['utilisateur'] as Map<String, dynamic>),
//         etat: json['etat'] != null ? json['etat'] as String : "",
//         category: Category.fromJson(json['category'] as Map<String, dynamic>),
//     );
//   }
// }


class Ressource {
  final int id;
  final String title;
  final String description;
  final DateTime creationDate;
  final DateTime? eventStart;
  final DateTime? eventEnd;
  final String file;
  final Category category;
  final User? utilisateur; // Utilisateur can be null
  final String etat;

  Ressource({
    required this.id,
    required this.title,
    required this.description,
    required this.creationDate,
    this.eventStart,
    this.eventEnd,
    required this.file,
    required this.category,
    this.utilisateur,
    required this.etat,
  });

  factory Ressource.fromJson(Map<String, dynamic> json) {
    return Ressource(
      id: json['id'],
      title: json['title'],
      description: json['description'],
      creationDate: DateTime.parse(json['creationDate']),
      eventStart: json['eventStart'] != null ? DateTime.parse(json['eventStart']) : null,
      eventEnd: json['eventEnd'] != null ? DateTime.parse(json['eventEnd']) : null,
      file: json['file'],
      category: Category.fromJson(json['category']),
      utilisateur: json['utilisateur'] != null ? User.fromJson(json['utilisateur']) : null,
      etat: json['etat'],
    );
  }
}