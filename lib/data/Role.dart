enum Role {
  ADMIN,
  MODO,
  SUPER_ADMIN,
  CITOYEN
}

extension RoleExtension on Role {
  String get value {
    switch (this) {
      case Role.ADMIN:
        return 'ADMIN';
      case Role.MODO:
        return 'MODO';
      case Role.SUPER_ADMIN:
        return 'SUPER_ADMIN';
      case Role.CITOYEN:
        return 'CITOYEN';
      default:
        throw Exception('Invalid role');
    }
  }

  static Role fromString(String value) {
    switch (value) {
      case 'ADMIN':
        return Role.ADMIN;
      case 'MODO':
        return Role.MODO;
      case 'SUPER_ADMIN':
        return Role.SUPER_ADMIN;
      case 'CITOYEN':
        return Role.CITOYEN;
      default:
        throw Exception('Invalid role string');
    }
  }

  static Role fromJson(dynamic json) {
    if (json is String) {
      return fromString(json);
    }
    throw Exception('Invalid role JSON');
  }
}
