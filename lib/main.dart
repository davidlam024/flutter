import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'data/Ressource.dart';



void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: RessourcesPage(), // Utilise la classe RessourcesPage comme page d'accueil
    );
  }
}

class RessourcesPage extends StatefulWidget {
  @override
  _RessourcesPageState createState() => _RessourcesPageState();
}

class _RessourcesPageState extends State<RessourcesPage> {
  List<Ressource> _ressources = [];

  @override
  void initState() {
    super.initState();
    _fetchRessources();
  }

  Future<void> _fetchRessources() async {
    final apiUrl = 'http://localhost:8080/ressources';
    try {
      final response = await http.get(Uri.parse(apiUrl));

      if (response.statusCode == 200) {
        final Map<String, dynamic> jsonData = json.decode(response.body);
        print(jsonData['resources'][0]);
        final List<dynamic> ressourcesData = jsonData['resources'];

        final List<Ressource> ressources = ressourcesData
            .map((resourceJson) => Ressource.fromJson(resourceJson))
            .toList();

        setState(() {
          _ressources = ressources.where((ressource) => ressource.etat == 'ACCEPTED').toList();

        });
      } else {
        print('Failed to load ressources: ${response.statusCode}');
      }
    } catch (e) {
      print('Error while fetching ressources: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Liste des Ressources'),
        centerTitle: true,
      ),
      backgroundColor: Color(0xFFF4EFE7),
      body: _ressources.isEmpty ?
            Center(
              child: Text("Aucune ressources en ligne"),
            )
          : ListView.builder(
            itemCount: _ressources.length,
            itemBuilder: (context, index) {
              final ressource = _ressources[index];
              return Container(
                // height: 200.0,
                margin: EdgeInsets.symmetric(vertical: 5),
                  child : Card(
                      elevation: 5,
                      child: ConstrainedBox(
                        constraints: BoxConstraints(minHeight: 120.0),
                        child: ListTile(
                          leading: SizedBox(
                            width: 200.0,
                            height: 300.0,
                            child: Image.network(
                              ressource.file,
                              fit: BoxFit.cover,
                            )
                          ),
                          title: Center(
                            child: Text(ressource.title),
                          ),
                          subtitle: Text(ressource.description),
                        )
                      )
                  )
                );
            },
          ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.insert_drive_file),
            label: 'Ressource',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add_circle),
            label: 'Ajouter',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.sports_kabaddi),
            label: 'Karaté',
          ),
        ],
      ),

    );
  }
}